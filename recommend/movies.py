from math import sqrt
from ..common.sim import *
from ..common.util import *

# Generate movie recommendations based on movie lens data

######################################################################################


# for a person: best other persons matches
def topMatches(prefs,person,n=5,similarity=pearson):
  scores=[(similarity(prefs[person],prefs[other]),other)
                  for other in prefs if other!=person]
  scores.sort()
  scores.reverse()
  return scores[0:n]

# for a person: recommendations by using a weighted average of every other persons's rankings
def recommendByPersons(prefs,person,similarity=pearson):
  totals={}
  simSums={}
  for other in prefs:
    if other==person: 
      continue
    sim=similarity(prefs[person],prefs[other])

    if sim<=0: 
      continue

    for item in prefs[other]:
      if item not in prefs[person] or prefs[person][item]==0:
        totals.setdefault(item,0)
        totals[item]+=prefs[other][item]*sim
        simSums.setdefault(item,0)
        simSums[item]+=sim

  rankings=[(total/simSums[item],item) for item,total in totals.items()]

  rankings.sort()
  rankings.reverse()
  return rankings

# util fuction: returns dictionary [item: (top n most similiar items)]
def calculateSimilarItems(prefs,n=10):
  result={}
  itemPrefs=transposeDict(prefs)
  c=0
  for item in itemPrefs:
    c+=1
    if c%100==0: 
      print "%d / %d" % (c,len(itemPrefs))
    scores=topMatches(itemPrefs,item,n=n,similarity=distance)
    result[item]=scores
  return result

# for a person: recommendations by using most similiar items
def recommendByItems(prefs,itemMatch,person):
  userRatings=prefs[person]
  scores={}
  totalSim={}
  for (item,rating) in userRatings.items():
    for (similarity,itemSim) in itemMatch[item]:
      # don't need to see movie again
      if itemSim in userRatings: 
        continue

      scores.setdefault(itemSim,0)
      scores[itemSim]+=similarity*rating

      totalSim.setdefault(itemSim,0)
      totalSim[itemSim]+=similarity

  rankings=[(score/totalSim[item],item) for item,score in scores.items()]

  rankings.sort()
  rankings.reverse()
  return rankings

# load data from movie lens data set
def loadMovieLens(path='ml-latest-small'):
  movies={}
  lineNum=0
  for line in open(path+'/movies.csv'):
    lineNum += 1
    # skip header
    if lineNum == 1: 
      continue
    (id,title)=line.split(',')[0:2]
    movies[id]=title
  
  prefs={}
  lineNum=0
  for line in open(path+'/ratings.csv'):
    lineNum += 1
    # skip header
    if lineNum == 1: 
      continue
    (user,movieid,rating,ts)=line.split(',')
    prefs.setdefault(user,{})
    prefs[user][movies[movieid]]=float(rating)
  return prefs


######################################################################################

# test data

critics={
  'Lisa Rose':{
    'Lady in the Water':2.5,
    'Snakes on a Plane':3.5,
    'Just My Luck': 3.0,
    'Superman Returns': 3.5,
    'You, Me and Dupree': 2.5, 
    'The Night Listener': 3.0
  },
  
  'Gene Seymour':{
    'Lady in the Water':3.0,
    'Snakes on a Plane':3.5,
    'Just My Luck':1.5,
    'Superman Returns':5.0,
    'The Night Listener':3.0,
    'You, Me and Dupree':3.5
  },
    
  'Michael Phillips':{
    'Lady in the Water':2.5,
    'Snakes on a Plane':3.0,
    'Superman Returns':3.5,
    'The Night Listener':4.0
  },

  'Claudia Puig':{
    'Snakes on a Plane':3.5,
    'Just My Luck':3.0,
    'The Night Listener':4.5,
    'Superman Returns':4.0,
    'You, Me and Dupree':2.5
  },

  'Mick LaSalle':{
    'Lady in the Water':3.0,
    'Snakes on a Plane':4.0,
    'Just My Luck':2.0,
    'Superman Returns':3.0,
    'The Night Listener':3.0,
    'You, Me and Dupree':2.0
  },
    
  'Jack Matthews':{
    'Lady in the Water':3.0,
    'Snakes on a Plane':4.0,
    'The Night Listener':3.0,
    'Superman Returns':5.0,
    'You, Me and Dupree':3.5
  },

  'Toby':{
    'Snakes on a Plane':1.5,
    'You, Me and Dupree':1.0,
    'Superman Returns':1.0
  }
}
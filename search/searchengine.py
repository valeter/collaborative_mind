import urllib2
from bs4 import *
from urlparse import urljoin
import sqlite3 as sqlite
from stemming.porter2 import stem
import re
from ..common.util import *
import nn

ignorewords=set(['the','of','to','a','and','in','is','it','for'])

class crawler:
	def __init__(self,dbname):
		self.con=sqlite.connect(dbname)
		self.cur=self.con.cursor()

	def __del__(self):
		self.con.close()

	def dbcommit(self):
		self.con.commit()

	# util function: get entity id, or create and get
	def getentryid(self,table,field,value,createnew=True):
		c=self.cur.execute("select rowid from %s where %s='%s'" % (table,field,value))
		res=c.fetchone()
		if res==None:
			c=self.cur.execute("insert into %s (%s) values ('%s')" % (table,field,value))
			return c.lastrowid
		else:
			return res[0]

	# index single page
	def addtoindex(self,url,soup):
		if self.isindexed(url): return
		print 'Indexing %s' % url

		text=self.gettextonly(soup)
		words=self.separatewords(text)

		urlid=self.getentryid('urllist','url',url)

		for i in range(len(words)):
			word=words[i]
			if word in ignorewords: continue
			wordid=self.getentryid('wordlist','word',word)
			self.cur.execute("insert into wordlocation(urlid,wordid,location) \
				values (%d,%d,%d)" % (urlid,wordid,i))

	# get text from HTML
	def gettextonly(self,soup):
		v=soup.string
		if v==None:
			c=soup.contents
			resulttext=''
			for t in c:
				subtext=self.gettextonly(t)
				resulttext+=subtext+'\n'
			return resulttext
		else:
			return v.strip()

	# get words from text
	def separatewords(self,text):
		splitter=re.compile('\\W+')
		return [stem(s.lower()) for s in splitter.split(text) if s!='']

	# returns True if page has already indexed
	def isindexed(self,url):
		u=self.cur.execute \
			("select rowid from urllist where url='%s'" % url).fetchone()
		if u!=None:
			v=self.cur.execute('select * from wordlocation where urlid=%d' % u[0]).fetchone()
			if v!=None: return True
		return False

	# add link from one page to another
	def addlinkref(self,urlFrom,urlTo,linkText):
		words=self.separatewords(linkText)
		fromid=self.getentryid('urllist','url',urlFrom)
		toid=self.getentryid('urllist','url',urlTo)
		if fromid==toid: return
		c=self.cur.execute("insert into link(fromid,toid) values (%d,%d)" % (fromid,toid))
		linkid=c.lastrowid
		for word in words:
			if word in ignorewords: continue
			wordid=self.getentryid('wordlist','word',word)
			self.cur.execute("insert into linkwords(linkid,wordid) values (%d,%d)" % (linkid,wordid))

	# starting from the given page list does bfs through the web
	def crawl(self,pages,depth=2):
		for i in range(depth):
			newpages=set()
			for page in pages:
				try:
					c=urllib2.urlopen(page)
				except:
					print 'Error on url %s' % url
					continue
				soup=BeautifulSoup(c.read(),"html.parser")
				self.addtoindex(page,soup)

				links=soup.find_all('a')
				for link in links:
					if (link.has_attr('href')):
						url=urljoin(page,link['href'])
						if url.find("'")!=-1: continue
						url=url.split('#')[0]
						if url[0:4]=='http' and not self.isindexed(url):
							newpages.add(url)
						linkText=self.gettextonly(link)
						self.addlinkref(page,url,linkText)

				self.dbcommit()
			pages=newpages

	# PageRank algorithm
	def calculatepagerank(self,iterations=20):
		self.cur.execute('drop table if exists pagerank')
		self.cur.execute('create table pagerank(urlid primary key,score)')

		# init all scores with 1
		self.cur.execute('insert into pagerank select rowid, 1.0 from urllist')
		self.dbcommit()

		for i in range(iterations):
			print 'Iteration %d' % i

			for (urlid,) in self.cur.execute('select rowid from urllist'):
				pr=0.15

				# for every page that contains link to current page
				for (linker) in self.cur.execute('select distinct fromid from link where toid=%d' % urlid):
					linkingpr=self.cur.execute('select score from pagerank where urlid=%d' % linker).fetchone()[0]
					linkingcount=self.cur.execute('select count(*) from link where fromid=%d' % linker).fetchone()[0]
					pr+=0.85*(linkingpr/linkingcount)

				self.cur.execute('update pagerank set score=%f where urlid=%d' % (pr,urlid))
			self.dbcommit()

	def createindextables(self):
		self.cur.execute('create table if not exists urllist(url)')
		self.cur.execute('create table if not exists wordlist(word)')
		self.cur.execute('create table if not exists wordlocation(urlid,wordid,location)')
		self.cur.execute('create table if not exists link(fromid integer,toid integer)')
		self.cur.execute('create table if not exists linkwords(wordid,linkid)')
		self.dbcommit()
		self.cur.execute('')
		self.cur.execute('create index if not exists wordidx on wordlist(word)')
		self.cur.execute('create index if not exists urlidx on urllist(url)')
		self.cur.execute('create index if not exists wordurlidx on wordlocation(wordid)')
		self.cur.execute('create index if not exists urltoidx on link(toid)')
		self.cur.execute('create index if not exists urlfromidx on link(fromid)')
		self.dbcommit()


class searcher:
	def __init__(self,dbname):
		self.con=sqlite.connect(dbname)
		self.cur=self.con.cursor()
		self.mynet=nn.searchnet(dbname)

	def __del__(self):
		self.con.close()

	# return pages with words
	def getmatchrows(self,q):
		fieldlist='w0.urlid'
		tablelist=''
		clauselist=''
		wordids=[]

		words=[stem(word.strip().lower()) for word in q.split(' ')]
		tablenumber=0

		for word in words:
			wordrow=self.cur.execute("select rowid from wordlist where word='%s'" % word).fetchone()
			if wordrow!=None:
				wordid=wordrow[0]
				wordids.append(wordid)
				if tablenumber>0:
					tablelist+=','
					clauselist+=' and '
					clauselist+='w%d.urlid=w%d.urlid and ' % (tablenumber-1,tablenumber)
				fieldlist+=',w%d.location' % tablenumber
				tablelist+='wordlocation w%d' % tablenumber
				clauselist+='w%d.wordid=%d' % (tablenumber,wordid)
				tablenumber+=1

		if (len(wordids)==0): return [],wordids

		fullquery='select %s from %s where %s' % (fieldlist,tablelist,clauselist)
		c=self.cur.execute(fullquery)
		rows=[row for row in c]

		return rows,wordids

	def trainnn(self,wordids,urlids,selectedurl):
		self.mynet.trainquery(wordids,urlids,selectedurl)

	# return scores for pages
	def getscoredlist(self,rows,wordids):
		totalscores=dict([(row[0],0) for row in rows])

		weights=[
			(1.0,self.frequencyscore(rows)),
			(1.3,self.locationscore(rows)),
			(0.5,self.distancescore(rows)),
			(1.0,self.pagerankscore(rows)),
			(1.0,self.linktextscore(rows,wordids)),
			(0.5,self.nnscore(rows,wordids))
		]
		for (weight,scores) in weights:
			for url in totalscores:
				totalscores[url]+=weight*scores[url]
		return totalscores

	# return url for urlid
	def geturlname(self,id):
		return self.cur.execute('select url from urllist where rowid=%d' % id).fetchone()[0]

	# make search query
	def query(self,q):
		rows,wordids=self.getmatchrows(q)
		scores=self.getscoredlist(rows,wordids)
		rankedscores=sorted([(score,url) for (url,score) in scores.items()], \
			reverse=1)
		for (score,urlid) in  rankedscores[0:10]:
			print '%f\t%s' % (score,self.geturlname(urlid))

		return wordids,[r[1] for r in rankedscores[0:10]]

	# score pages by word frequency
	def frequencyscore(self,rows):
		counts=dict([(row[0],0) for row in rows])
		for row in rows: counts[row[0]]+=1
		return normalizeDictValues(counts)

	# score pages by word location in text
	def locationscore(self,rows):
		locations=dict([(row[0],1000000) for row in rows])
		for row in rows:
			loc=sum(row[1:])
			if loc<locations[row[0]]: locations[row[0]]=loc
		return normalizeDictValues(locations,reverse=True)

	# score pages by distance between words in text
	def distancescore(self,rows):
		if len(rows[0])<=2: return dict([(row[0],1.0) for row in rows])

		mindistance=dict([(row[0],1000000) for row in rows])

		for row in rows:
			dist=sum([abs(row[i]-row[i-1]) for i in range(2,len(row))])
			if dist<mindistance[row[0]]: mindistance[row[0]]=dist
		return normalizeDictValues(mindistance,reverse=True)

	# score pages by number of inbound links
	def inboundlinkscore(self,rows):
		uniqueurls=set([row[0] for row in rows])
		inboundcount=dict([(u,self.cur.execute('select count(*) from link where toid=%d' % u).fetchone()[0]) \
			for u in uniqueurls])
		return normalizeDictValues(inboundcount)

	# score pages by PageRank algorithm
	def pagerankscore(self,rows):
		pageranks=dict([(row[0],self.cur.execute('select score from pagerank where urlid=%d' % row[0]).fetchone()[0]) \
			for row in rows])
		return normalizeDictValues(pageranks)

	# score pages by text in it's urls and PageRank score
	def linktextscore(self,rows,wordids):
		linkscores=dict([(row[0],0) for row in rows])
		for wordid in wordids:
			c=self.cur.execute('select link.fromid,link.toid from linkwords,link \
				where wordid=%d and linkwords.linkid=link.rowid' % wordid)
			for (fromid,toid) in c:
				if toid in linkscores:
					pr=self.cur.execute('select score from pagerank where urlid=%d' % fromid).fetchone()[0]
					linkscores[toid]+=pr
		return normalizeDictValues(linkscores)

	# score pages by neural network (with reverse coupling)
	def nnscore(self,rows,wordids):
		urlids=[urlid for urlid in set([row[0] for row in rows])]
		nnres=self.mynet.getresult(wordids,urlids)
		scores=dict([(urlids[i],nnres[i]) for i in range(len(urlids))])
		return normalizeDictValues(scores)
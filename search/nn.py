from math import tanh
import sqlite3 as sqlite
import os


class searchnet:
	def __init__(self,dbname):
		self.con=sqlite.connect(dbname)
		self.cur=self.con.cursor()

	def __del__(self):
		self.con.close()

	# returns predicted range for urls in query result
	def getresult(self,input_ids,output_ids):
		self.setupnetwork(input_ids,output_ids)
		return self.feedforward()

	# train nn on query result
	def trainquery(self,input_ids,output_ids,selected_ids=[]):
		self.generatehiddennode(input_ids,output_ids)
		self.setupnetwork(input_ids,output_ids)
		self.feedforward()
		targets=[0.0]*len(output_ids)
		for selected_id in selected_ids:
			targets[output_ids.index(selected_id)]=1.0
		error=self.backPropagate(targets)
		self.updatedatabase()

	# back propagation procedure for nn training
	# assuming that hidden nodes is ordered by distance from input asc
	def backPropagate(self,targets,N=0.5):
		# calculating corrections for output layer
		output_deltas=[0.0]*len(self.output_ids)
		for k in range(len(self.output_ids)):
			error=targets[k]-self.ao[k]
			output_deltas[k]=dtanh(self.ao[k])*error

		# calculating corrections for hidden layer
		hidden_deltas=[0.0]*len(self.hiddenids)
		for j in reversed(range(len(self.hiddenids))):
			error=0.0
			for k in range(len(self.output_ids)):
				error+=output_deltas[k]*self.wo[j][k]
			hidden_deltas[j]=dtanh(self.ah[j])*error

		# recalculate weights between hidden and output layer
		for j in range(len(self.hiddenids)):
			for k in range(len(self.output_ids)):
				change=output_deltas[k]*self.ah[j]
				self.wo[j][k]=self.wo[j][k]+N*change

		# recalculate weights between input and hidden layer
		for i in range(len(self.input_ids)):
			for j in range(len(self.hiddenids)):
				change=hidden_deltas[j]*self.ai[i]
				self.wi[i][j]=self.wi[i][j]+N*change

	def updatedatabase(self):
		for i in range(len(self.input_ids)):
			for j in range(len(self.hiddenids)):
				self.setstrength(self.input_ids[i],self.hiddenids[j],'in',self.wi[i][j])
		
		for j in range(len(self.hiddenids)):
			for k in range(len(self.output_ids)):
				self.setstrength(self.hiddenids[j],self.output_ids[k],'out',self.wo[j][k])
		self.con.commit()

	# get reaction on input
	# assuming that hidden nodes is ordered by distance from input asc
	def feedforward(self):
		# input signals
		for i in range(len(self.input_ids)):
			self.ai[i]=1.0

		# activation of hidden nodes
		for j in range(len(self.hiddenids)):
			s=0.0
			for i in range(len(self.input_ids)):
				s+=self.ai[i]*self.wi[i][j]
			self.ah[j]=tanh(s)

		# activation of output signals
		for k in range(len(self.output_ids)):
			s=0.0
			for j in range(len(self.hiddenids)):
				s+=self.ah[j]*self.wo[j][k]
			self.ao[k]=tanh(s)

		return self.ao[:]

	# load part of nn from db
	def setupnetwork(self,input_ids,output_ids):
		self.input_ids=input_ids
		self.hiddenids=self.gethiddenids(input_ids,output_ids)
		self.output_ids=output_ids

		# output signals
		self.ai=[1.0]*len(self.input_ids)
		self.ah=[1.0]*len(self.hiddenids)
		self.ao=[1.0]*len(self.output_ids)

		# creating weight matrix
		self.wi=[[self.getstrength(in_id,h_id,'in') for h_id in self.hiddenids] for in_id in self.input_ids]
		self.wo=[[self.getstrength(h_id,out_id,'out') for out_id in self.output_ids] for h_id in self.hiddenids]

	# get all hidden nodes id's for query input and results
	def gethiddenids(self,input_ids,output_ids):
		l1={}
		for in_id in input_ids:
			c=self.cur.execute('select toid from input_to_hidden where fromid=\'%s\'' % in_id)
			for row in c:
				l1[row[0]]=1
		for out_id in output_ids:
			c=self.cur.execute('select fromid from hidden_to_output where toid=\'%s\'' % out_id)
			for row in c:
				l1[row[0]]=1

		return l1.keys()

	# generates hidden node for new query
	def generatehiddennode(self,input_ids=[],output_ids=[]):
		if len(input_ids)>10 or len(input_ids)==0: return None
		createkey='_i_'.join(sorted([str(wi) for wi in input_ids]))
		res=self.cur.execute("select rowid from hidden_node where create_key='%s'" % createkey).fetchone()
		if res==None:
			c=self.cur.execute("insert into hidden_node(create_key) values ('%s')" % createkey)
			hiddenid=c.lastrowid

			# add connections to input and output
			for in_id in input_ids:
				self.setstrength(in_id,hiddenid,'in',1.0/len(input_ids))
			for out_id in output_ids:
				self.setstrength(hiddenid,out_id,'out',0.1)

			self.con.commit()

	# get neuron connection strength
	def getstrength(self,fromid,toid,layer):
		if layer=='in': table='input_to_hidden'
		else: table='hidden_to_output'
		res=self.cur.execute('select strength from %s where fromid=\'%s\' and toid=\'%s\'' % (table,fromid,toid)).fetchone()
		if res==None:
			if layer=='in': return -0.2
			else: return 0
		return res[0]

	# set neuron connection strength
	def setstrength(self,fromid,toid,layer,strength):
		if layer=='in': table='input_to_hidden'
		else: table='hidden_to_output'
		res=self.cur.execute('select rowid from %s where fromid=\'%s\' and toid=\'%s\'' % (table,fromid,toid)).fetchone()
		if res==None:
			self.cur.execute('insert into %s (fromid,toid,strength) values (\'%s\',\'%s\',%f)' % (table,fromid,toid,strength))
		else:
			rowid=res[0]
			self.cur.execute('update %s set strength=%f where rowid=%d' % (table,strength,rowid))

	def createtables(self):
		self.cur.execute('create table if not exists hidden_node(create_key)')
		self.cur.execute('create table if not exists input_to_hidden(fromid,toid,strength)')
		self.cur.execute('create table if not exists hidden_to_output(fromid,toid,strength)')
		self.con.commit()


def samplenn():
	filename='_samplenn_.db'
	if os.path.exists(filename):
		os.remove(filename)

	nn=searchnet(filename)
	nn.createtables()
	nn.trainquery(input_ids=['quick','brown','fox'],output_ids=['good','bad'],selected_ids=['good'])
	nn.trainquery(input_ids=['quick','rabbit','is','gray'],output_ids=['good','bad'],selected_ids=['good'])
	nn.trainquery(input_ids=['fast','money','in','online','casino'],output_ids=['good','bad'],selected_ids=['bad'])
	nn.trainquery(input_ids=['pay','money','for','nothing'],output_ids=['good','bad'],selected_ids=['bad'])
	nn.trainquery(input_ids=['gray','scheme','for','best'],output_ids=['good','bad'],selected_ids=['bad'])
	nn.trainquery(input_ids=['look','at','the','door'],output_ids=['good','bad'],selected_ids=['good'])
	return nn


# util function
def dtanh(y):
	return 1-y*y
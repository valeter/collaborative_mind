# -*- coding: utf-8 -*-
import re
import math
import sqlite3 as sqlite
import csv

headers=[
	"TEXT_IS_NULL","PRO_IS_NULL","CONTRA_IS_NULL","TEXT","PRO","CONTRA",
	"SHOP_ID","GRADE","ORDER_ID","RESOLVED","IP","YANDEXUID","SESSIONUID","FUID",
	"HTTP_HEADERS","CONTEXT"
]

text_headers=set(["TEXT","PRO","CONTRA","HTTP_HEADERS","CONTEXT"])

def getwords(doc):
	splitter=re.compile('\\W*')
	words=[s.lower() for s in splitter.split(doc) if len(s)>2 and len(s)<20]
	return dict([(w,1) for w in words])

def getGradeFeatures(grade):
	splitter=re.compile('\\W*')
	result=[]
	for i in range(0,len(headers)):
		if headers[i] in text_headers:
			result.extend([headers[i]+'_'+s.lower() for s in splitter.split(grade[i]) if len(s)>2 and len(s)<20])
		else:
			result.append(headers[i]+'_'+str(grade[i]))
	return dict([(r,1) for r in result])

# reads grades.csv file, returns tuple (row names array, column names array, data array)
def getGrades():
	cl=fisherclassifier(getGradeFeatures)

	filename='/Users/valter/grades.csv'
	i=0
	with open(filename, 'rb') as csvfile:
		gradereader=csv.reader(csvfile,delimiter=',', quotechar='"')
		for grade in gradereader:
			cl.train(grade, grade[len(headers) + 1])
			if (i%1000==0): print 'Processing line %d. Finished on %.2f' % (i + 1, float(i)*100.0/332426.0)
			i+=1
	return cl

class classifier:
	def __init__(self,getfeatures,dbname=None,filename=None):
		self.getfeatures=getfeatures
		self.fc={}
		self.cc={}
		if dbname!=None:
			self.setdb(dbname)

	def __del__(self):
		if hasattr(self,'con'):
			self.con.close()

	# Соединиться с БД
	def setdb(self,dbname):
		self.con=sqlite.connect(dbname)
		self.cur=self.con.cursor()
		self.cur.execute('create table if not exists fc(feature,category,count)')
		self.cur.execute('create table if not exists cc(category,count)')
		self.con.commit()

	# Классифицировать документ
	def classify(self,item,default=None):
		pass

	# Обучение классификатора
	def train(self,item,cat):
		features=self.getfeatures(item)
		for f in features:
			self.incf(f,cat)
		self.incc(cat)

	# Увеличить счетчик пар признак/категория
	def incf(self,f,cat):
		if hasattr(self,'con'):
			count=self.fcount(f,cat)
			if count==0:
				self.cur.execute("insert into fc values ('%s','%s',1)" % (f,cat))
			else:
				self.cur.execute("update fc set count=%d where feature='%s' and category='%s'" % (count+1,f,cat))
			self.con.commit()
		else:
			self.fc.setdefault(f,{})
			self.fc[f].setdefault(cat,0)
			self.fc[f][cat]+=1

	# Увеличить счетчик применений категории
	def incc(self,cat):
		if hasattr(self,'con'):
			count=self.catcount(cat)
			if count==0:
				self.cur.execute("insert into cc values ('%s',1)" % (cat))
			else:
				self.cur.execute("update cc set count=%d where category='%s'" % (count+1,cat))
			self.con.commit()
		else:
			self.cc.setdefault(cat,0)
			self.cc[cat]+=1

	# Сколько раз признак появлялся в категории
	def fcount(self,f,cat):
		if hasattr(self,'con'):
			res=self.con.execute('select count from fc where feature="%s" and category="%s"' % (f,cat)).fetchone()
			if res==None: return 0.0
			else: return float(res[0])
		else:
			if f in self.fc and cat in self.fc[f]:
				return float(self.fc[f][cat])
			return 0.0

	# Сколько образцов отнесено к данной категории
	def catcount(self,cat):
		if hasattr(self,'con'):
			res=self.cur.execute('select count from cc where category="%s"' %(cat)).fetchone()
			if res==None: return 0.0
			else: return float(res[0])
		else:
			if cat in self.cc:
				return float(self.cc[cat])
			return 0

	# Общее число образцов
	def totalcount(self):
		if hasattr(self,'con'):
			res=self.cur.execute('select sum(count) from cc').fetchone(); 
			if res==None: return 0
			else: return res[0]
		else:
			return sum(self.cc.values())

	# Список всех категорий
	def categories(self):
		if hasattr(self,'con'):
			cur=self.cur.execute('select category from cc');
			return [d[0] for d in cur]
		else:
			return self.cc.keys()

	# Возвращает вероятность появления признака в категории
	def fprob(self,f,cat):
		if self.catcount(cat)==0: return 0
		# Общее число раз, когда данный признак появллся в этой категории,
		# дклим на количество образцов в той де категории
		return self.fcount(f,cat)/self.catcount(cat)

	# Возвращает взвешенную вероятность появления признака в категории. По умолчанию 0.5
	def weighttedprob(self,f,cat,prf,weight=1.0,ap=0.5):
		basicprob=prf(f,cat)
		totals=sum([self.fcount(f,c) for c in self.categories()])
		bp=((weight*ap)+(totals*basicprob))/(weight+totals)
		return bp


# Наивная Байесовская классификация (предполагаем, что признаки независимы, хотя обычно это не так)
class naivebayes(classifier):
	def __init__(self,getfeatures,dbname=None,filename=None):
		classifier.__init__(self,getfeatures,dbname,filename)
		self.thresholds={}

	# Классифицировать документ
	def classify(self,item,default=None):
		probs={}
		# Найти категорию с максимальной вероятностью
		max=0.0
		for cat in self.categories():
			probs[cat]=self.prob(item,cat)
			if probs[cat]>max:
				max=probs[cat]
				best=cat

		# Убедиться что найденная вероятность >  чем threshold*следующая по величине
		for cat in self.categories():
			if cat==best: continue
			if probs[cat]*self.getthreshold(best)>probs[best]: return default
		return best

	# Установить пороговое значение для категории
	def setthreshold(self,cat,t):
		self.thresholds[cat]=t

	# Получить пороговое значение для категории
	def getthreshold(self,cat):
		if cat not in self.thresholds: return 1.0
		return float(self.thresholds[cat])

	def docprob(self,item,cat):
		features=self.getfeatures(item)
		p=1.0
		for f in features: p*=self.weighttedprob(f,cat,self.fprob)
		return p

	# Вероятность принадлежности документа к категории (формула Байеса)
	def prob(self,item,cat):
		catprob=self.catcount(cat)/self.totalcount()
		docprob=self.docprob(item,cat)
		return docprob*catprob


# Классификация методом Фишера
class fisherclassifier(classifier):
	def __init__(self,getfeatures,dbname=None,filename=None):
		classifier.__init__(self,getfeatures,dbname,filename)
		self.minimums={}

	# Классифицировать документ
	def classify(self,item,default=None):
		best=default
		max=0.0
		for c in self.categories():
			p=self.fisherprob(item,c)
			# Проверяем что значение больше минимума
			if p>self.getminimum(c) and p>max:
				best=c
				max=p
		return best

	# Установить минимальный порог вероятности для категории
	def setminimum(self,cat,min):
		self.minimums[cat]=min

	# Получить минимальный порог вероятности для категории
	def getminimum(self,cat):
		if cat not in self.minimums: return 0.0
		return self.minimums[cat]

	# Можно использовать вместо метода fprob
	def cprob(self,f,cat):
		# Частота появления данног признака в категории
		clf=self.fprob(f,cat)
		if clf==0: return 0
		# Частота появления данного признака во всех категориях
		freqsum=sum([self.fprob(f,c) for c in self.categories()])
		return clf/freqsum

	# Вероятность принадлежности документа к категории по Фишеру
	def fisherprob(self,item,cat):
		# Перемножить все вероятности
		p=1
		features=self.getfeatures(item)
		for f in features:
			p*=(self.weighttedprob(f,cat,self.cprob))

		fscore=-2*math.log(p)
		return self.invchi2(fscore,len(features)*2)

	def invchi2(self,chi,df):
		m=chi/2.0
		sum=term=math.exp(-m)
		for i in range(1, df/2):
			term*=m/i
			sum+=term
		return min(sum, 1.0)


def sampletrain(cl):
	cl.train('Nobody owns the water.','good')
	cl.train('the quick rabbit jumps fences','good')
	cl.train('buy pharmaceuticals now','bad')
	cl.train('make quick money at the online casino','bad')
	cl.train('the quick brown fox jumps','good')
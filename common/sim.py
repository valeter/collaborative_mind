from math import sqrt

# Input for all fuctions: dictionaries

######################################################################################


# util function: returns dictionary with similiar items for both vectors
def get_sim(v1,v2):
  si={}
  for item in v1: 
    if item in v2: si[item]=1
  return si

# euclidian distance similarity
def distance(v1,v2):
  si=get_sim(v1,v2)

  if len(si)==0: return 0

  sum_of_squares=sum([pow(v1[item]-v2[item],2)
                      for item in v1 if item in v2])

  return 1/(1+sum_of_squares)

# pearson similarity (correlation coefficient)
def pearson(v1,v2):
  si=get_sim(v1,v2)

  if len(si)==0: return 0

  n=len(si)
  if n==0: return 0
  
  sum1=sum([v1[it] for it in si])
  sum2=sum([v2[it] for it in si])
  
  sum1Sq=sum([pow(v1[it],2) for it in si])
  sum2Sq=sum([pow(v2[it],2) for it in si]) 
  
  pSum=sum([v1[it]*v2[it] for it in si])
  
  num=pSum-(sum1*sum2/n)
  den=sqrt((sum1Sq-pow(sum1,2)/n)*(sum2Sq-pow(sum2,2)/n))
  if den==0: return 0

  # -1 .. 1 -> 0 .. 1
  return (1+num/den)/2 

# jaccard similarity
def jaccard(v1,v2):
  si=get_sim(v1,v2)

  if len(si)==0: return 0

  sumMin=sum([min(v1[it],v2[it]) for it in si])
  sumMax=sum([max(v1[it],v2[it]) for it in si])

  return sumMin/sumMax

# tanimoto similarity
def tanimoto(v1,v2):
  si=get_sim(v1,v2)

  if len(si)==0: return 0

  sumMult=sum([v1[it]*v2[it] for it in si])
  sumSq1=sum([pow(v1[it],2) for it in si])
  sumSq2=sum([pow(v2[it],2) for it in si])

  return sumMult/(sumSq1+sumSq2-sumMult)
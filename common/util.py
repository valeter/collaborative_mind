# util fuction: transpose dictionary
def transposeDict(d):
  result={}
  for k in d:
    for v in d[k]:
      result.setdefault(v,{})      
      result[v][k]=d[k][v]
  return result

# util fuction: transpose matrix
def transposeMatrix(m):
  result=[]
  for i in range(len(m[0])):
    newrow=[m[j][i] for j in range(len(m))]
    result.append(newrow)
  return result

# reads tsv file, returns tuple (row names array, column names array, data array)
def readTableFile(filename):
  lines=[line for line in file(filename)]
  
  colnames=lines[0].strip().split('\t')[1:]
  rownames=[]
  data=[]
  for line in lines[1:]:
    p=line.strip().split('\t')
    rownames.append(p[0])
    data.append([float(x) for x in p[1:]])
  return rownames,colnames,data

def normalizeDictValues(scores,reverse=False):
  vsmall=0.0000001
  if reverse:
    minscore=min(scores.values())
    return dict([(u,float(minscore)/max(vsmall,l)) for (u,l) in scores.items()])
  else:
    maxscore=max(scores.values())
    if maxscore==0: maxscore=vsmall
    return dict([(u,float(c)/maxscore) for (u,c) in scores.items()])
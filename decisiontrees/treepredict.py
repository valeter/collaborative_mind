# -*- coding: utf-8 -*-

from PIL import Image,ImageDraw
import zillow

# Вычислить частоту вхождения каждого результата в множество строк (результат - последний столбец в каждой строке)
def uniquecounts(rows):
	results={}
	for row in rows:
		r=row[len(row)-1]
		results.setdefault(r,0)
		results[r]+=1
	return results

# Коэффициент Джини. Вероятность того, что случайный образец не принадлежит к случайной категории
def giniimpurity(rows):
	total=len(rows)
	counts=uniquecounts(rows)
	imp=0.0
	for k1 in counts:
		p1=float(counts[k1])/total
		for k2 in counts:
			if k1==k2: continue
			p2=float(counts[k2])/total
			imp+=p1*p2
	return imp

# Значение энтропии на множестве. Вычисляется как сумма p(x)*log(p(x)) по всем различным результатам
def entropy(rows):
	from math import log
	log2=lambda x:log(x)/log(2)
	results=uniquecounts(rows)

	ent=0.0
	for r in results.keys():
		p=float(results[r])/len(rows)
		ent-=p*log2(p)
	return ent

# Дисперсия (если результаты классификации - числовые)
def variance(rows):
	if len(rows)==0: return 0
	data=[float(row[len(row)-1]) for row in rows]
	mean=sum(data)/len(data)
	variance=sum([(d-mean)**2 for d in data])/len(data)
	return variance


# Классификация с восполнением данных
def mdclassify(observation,tree):
	if tree.results!=None:
		return tree.results
	else:
		v=observation[tree.col]
		if v==None:
			tr,fr=mdclassify(observation,tree.tb),mdclassify(observation,tree.fb)
			tcount=sum(tr.values())
			fcount=sum(fr.values())
			tw=float(tcount)/(tcount+fcount)
			fw=float(fcount)/(tcount+fcount)
			result={}
			for k,v in tr.items(): result[k]=v*tw
			for k,v in fr.items(): result[k]=v*fw
			return result
		else:
			branch=None
			if isinstance(v,int) or isinstance(v,float):
				if v>=tree.value: branch=tree.tb
				else: branch=tree.fb
			else:
				if v==tree.value: branch=tree.tb
				else: branch=tree.fb
			return mdclassify(observation,branch)


# Классификация наблюдения в соответствии с деревом решений
def classify(observation,tree):
	if tree.results!=None:
		return tree.results
	else:
		v=observation[tree.col]
		branch=None
		if isinstance(v,int) or isinstance(v,float):
			if v>=tree.value: branch=tree.tb
			else: branch=tree.fb
		else:
			if v==tree.value: branch=tree.tb
			else: branch=tree.fb
		return classify(observation,branch)


# Прюнинг (сокращение) дерева. Проверяем пары узлов с общим предком. 
# Смотрим - насколько увеличивается энтропия при их объединении, если меньше порога - объединяем
def prune(tree,mingain):
	# Если ветви не листовые, вызвать рекурсивно
	if tree.tb.results==None:
		prune(tree.tb,mingain)
	if tree.fb.results==None:
		prune(tree.fb,mingain)

	# Если обе подветви заканчиваются листьями, смотрим - нужно ли их объединить
	if tree.tb.results!=None and tree.fb.results!=None:
		# Строим объединенный набор данных
		tb,fb=[],[]
		for v,c in tree.tb.results.items():
			tb+=[[v]]*c
		for v,c in tree.fb.results.items():
			fb+=[[v]]*c

		# Вычисляем, насколько уменьшилась энтропия
		delta=entropy(tb+fb)-((entropy(tb)+entropy(fb))/2)
		if delta<mingain:
			# Объединить ветви
			tree.tb,tree.fb=None,None
			tree.results=uniquecounts(tb+fb)


# Построение дерева решений
def buildtree(rows,scoref=entropy):
	rows=[row for row in rows if row is not None]

	if len(rows)==0: return decisionnode()
	current_score=scoref(rows)

	# Инициализируем переменные для выбора наилучшего критерия
	best_gain=0.0
	best_criteria=None
	best_sets=None

	column_count=len(rows[0])-1
	for col in range(0,column_count):
		# Создать список различных значений в этом столбце
		column_values={}
		for row in rows:
			column_values[row[col]]=1
		# Пробуем разбить множество строк по каждому значению из этого столбца
		for value in column_values.keys():
			(set1,set2)=divideset(rows,col,value)

			# Информационный выигрыш
			p=float(len(set1)/len(rows))
			gain=current_score-p*scoref(set1)-(1-p)*scoref(set2)
			if gain>best_gain and len(set1)>0 and len(set2)>0:
				best_gain=gain
				best_criteria=(col,value)
				best_sets=(set1,set2)

	# Создаем подветви
	if best_gain>0:
		trueBranch=buildtree(best_sets[0])
		falseBranch=buildtree(best_sets[1])
		return decisionnode(col=best_criteria[0],value=best_criteria[1],tb=trueBranch,fb=falseBranch)
	else:
		return decisionnode(results=uniquecounts(rows))


# Вывод дерева решений
def printtree(tree,indent=''):
	# Это листовой узел?
	if tree.results!=None:
		print str(tree.results)
	else:
		# Печатаем критерий
		print str(tree.col)+':'+str(tree.value)+'? '

		# Печатаем ветви
		print indent+'T->',
		printtree(tree.tb,indent+' ')
		print indent+'F->',
		printtree(tree.fb,indent+' ')


class decisionnode:
	def __init__(self,col=-1,value=None,results=None,tb=None,fb=None):
		self.col=col # Индекс столбца проверяемого условия
		self.value=value # Значение, котоому должно соответствовать значение в столбце, чтобы результат был = true
		self.results=results # Словарь результатов для этой ветви. None для всех не-листьев
		self.tb=tb # Экземпляры класс decisionnode, в который происходит переход в случае, если результаты true или false соотв-но
		self.fb=fb #

# Разбиение множества по указанному столбцу. Может обрабатывать как числовые, так и дискретные значения
def divideset(rows,column,value):
	# Создать функцию, которая сообщит, относится ли строка к первой группе (true) или ко второй (false)
	split_function=None
	if isinstance(value,int) or isinstance(value,float):
		split_function=lambda row:row[column]>=value
	else:
		split_function=lambda row:row[column]==value

	# Разбить множество строк на две части и вернуть их
	set1=[row for row in rows if split_function(row)]
	set2=[row for row in rows if not split_function(row)]
	return (set1,set2)


# Рисование дерева

def getwidth(tree):
	if tree.tb==None and tree.fb==None: return 1
	return getwidth(tree.tb)+getwidth(tree.fb)

def getdepth(tree):
	if tree.tb==None and tree.fb==None: return 0
	return max(getdepth(tree.tb),getdepth(tree.fb))+1

def drawtree(tree,gif='tree.gif'):
	w=getwidth(tree)*100
	h=getdepth(tree)*100+120

	img=Image.new('RGB',(w,h),(255,255,255))
	draw=ImageDraw.Draw(img)

	drawnode(draw,tree,w/2,20)
	img.save(gif,'GIF')

def drawnode(draw,tree,x,y):
	if tree.results==None:
		# Вычислить ширину каждой ветви
		w1=getwidth(tree.fb)*100
		w2=getwidth(tree.tb)*100

		# Вычислить, сколько места нужно данному узлу
		left=x-(w1+w2)/2
		right=x+(w1+w2)/2

		# Строка с условием
		draw.text((x-20,y-10),str(tree.col)+':'+str(tree.value),(0,0,0))

		# Линии к дочерним узлам
		draw.line((x,y,left+w1/2,y+100),fill=(255,0,0))
		draw.line((x,y,right-w2/2,y+100),fill=(255,0,0))

		# Дочерние узлы
		drawnode(draw,tree.fb,left+w1/2,y+100)
		drawnode(draw,tree.tb,right-w2/2,y+100)
	else:
		txt=' \n'.join(['%s:%d'%v for v in tree.results.items()])
		draw.text((x-20,y),txt,(0,0,0))


my_data=[
	['slashdot','USA','yes',18,'None'],
	['google', 'France','yes',23,'Premium'],
	['digg','USA','yes',24,'Basic'],
	['kiwitobes','France','yes',23,'Basic'],
	['google','UK','no',21,'Premium'],
	['(direct)','New Zealand','no',12,'None'],
	['(direct)','UK','no',21,'Basic'],
	['google','USA','no',24,'Premium'],
	['slashdot','France','yes',19,'None'],
	['digg','USA','no',18,'None'],
	['google','UK','no',18,'None'],
	['kiwitobes','UK','no',19,'None'],
	['digg','New Zealand','yes',12,'Basic'],
	['slashdot','UK','no',21,'None'],
	['google','UK','yes',18,'Basic'],
	['kiwitobes','France','yes',19,'Basic']
]
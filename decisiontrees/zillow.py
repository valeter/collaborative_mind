# -*- coding: utf-8 -*-

import xml.dom.minidom as xml
import urllib2

zwskey='X1-ZWz198mz5zifbf_8uv2u'

def getpricelist(filename='addresslist.txt'):
    l1=[]
    for line in file(filename):
        data=getaddressdata(line.strip(),'Cambridge,MA')
        l1.append(data)
    return l1

def getaddressdata(address,city):
    escad=address.replace(' ','+')

    url='https://www.zillow.com/webservice/GetDeepSearchResults.htm?'
    url+='zws-id=%s&address=%s&citystatezip=%s'%(zwskey,escad,city)

    doc=xml.parseString(urllib2.urlopen(url).read())
    code=doc.getElementsByTagName('code')[0].firstChild.data

    # 0 - успех
    if code!='0': return None

    try:
        zipcode=doc.getElementsByTagName('zipcode')[0].firstChild.data
        use=doc.getElementsByTagName('useCode')[0].firstChild.data
        year=doc.getElementsByTagName('yearBuilt')[0].firstChild.data
        bath=doc.getElementsByTagName('bathrooms')[0].firstChild.data
        bed=doc.getElementsByTagName('bedrooms')[0].firstChild.data
        rooms=doc.getElementsByTagName('totalRooms')[0].firstChild.data
        price=doc.getElementsByTagName('amount')[0].firstChild.data
    except:
        return None

    return (zipcode,use,int(year),float(bath),int(bed),int(rooms),price)
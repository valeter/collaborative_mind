import feedparser
import re

# returns tuple (RSS feed title, dictionary of word counts in feed)
def getwordcounts(url):
  d=feedparser.parse(url)
  wc={}

  for e in d.entries:
    if 'title' in e: title=e.title
    else: title=''

    if 'summary' in e: summary=e.summary
    elif 'content' in e: summary=e.content
    elif 'description' in e: summary=e.description
    else: summary=''

    words=getwords(title + ' ' + summary)
    for word in words:
      wc.setdefault(word,0)
      wc[word]+=1

  if 'feed' in d and 'title' in d.feed and len(d.feed.title.strip()) > 0: 
    feedTitle=d.feed.title.strip()
  else: 
    feedTitle=url
  feedTitle=re.sub(r'[^\x00-\x7F]+','', feedTitle).strip()
  feedTitle=re.sub(r'[\t]+',' ', feedTitle).strip()

  print 'Title: [%s]' % feedTitle
  return feedTitle,wc

# returns array of words from hmtl
def getwords(html):
  txt=re.compile(r'<[^>]+>').sub('',html)
  words=re.compile(r'[^A-Z^a-z]+').split(txt)
  return [word.lower() for word in words if word!='']


apcount={}
wordcounts={}
feedlist=[line for line in file('feedlist.txt')]
for feedurl in feedlist:
  try:
    print 'Parsing feed %s' % feedurl.strip()
    title,wc=getwordcounts(feedurl)
    wordcounts[title]=wc
    for word,count in wc.items():
      apcount.setdefault(word,0)
      if count>1:
        apcount[word]+=1
    print 'Successfully parsed feed %s' % feedurl
  except IOError as e:
    print 'Failed to parse feed %s' % feedurl

wordlist=[]
for w,bc in apcount.items():
  frac=float(bc)/len(feedlist)
  if frac>0.1 and frac<0.5:
    wordlist.append(w)

filename='blogdata.txt'
print '\nFinished parsing. Writing stat data to file %s' % filename

out=file(filename,'w')
out.write('Blog')
for word in wordlist: out.write('\t%s' % word)
out.write('\n')
for blog,wc in wordcounts.items():
  out.write(blog)
  for word in wordlist:
    if word in wc: out.write('\t%d' % wc[word])
    else: out.write('\t0')
  out.write('\n')
from math import sqrt
from PIL import Image,ImageDraw
from ..common.sim import *
from ..common.util import *
import random

# Hierarchical and K-means clusterisation of RSS feeds

######################################################################################


# hierachical cluster
class bicluster:
  def __init__(self,vec,left=None,right=None,distance=0.0,id=None):
    self.left=left
    self.right=right
    self.vec=vec
    self.id=id
    self.distance=distance

# util function: get distance between clusters
def getDistance(vec1,vec2,distance):
  return 1.0-distance({i:vec1[i] for i in range(len(vec1))}, {i:vec2[i] for i in range(len(vec2))})

# transposes and hierarchically clusterizes data. Returns bicluster object
def rhcluster(rows,distance=pearson):
  return hcluster(transposeMatrix(rows),distance)

# hierarchically clusterizes data. Returns bicluster object
def hcluster(rows,distance=pearson):
  distances={}
  currentclustid=-1

  # initial clusters is rows
  clust=[bicluster(rows[i],id=i) for i in range(len(rows))]

  while len(clust)>1:
    lowestpair=(0,1)
    closest=getDistance(clust[0].vec,clust[1].vec,distance)

    # find pair with minimum distance
    for i in range(len(clust)):
      for j in range(i+1,len(clust)):
        if (clust[i].id,clust[j].id) not in distances: 
          distances[(clust[i].id,clust[j].id)]=getDistance(clust[i].vec,clust[j].vec,distance)

        d=distances[(clust[i].id,clust[j].id)]

        if d<closest:
          closest=d
          lowestpair=(i,j)

    # calculate average vector between 2 clusters
    mergevec=[(clust[lowestpair[0]].vec[i]+clust[lowestpair[1]].vec[i])/2.0 for i in range(len(clust[0].vec))]

    newcluster=bicluster(
      mergevec,
      left=clust[lowestpair[0]],
      right=clust[lowestpair[1]],
      distance=closest,
      id=currentclustid)

    # new cluster ids < 0
    currentclustid-=1
    del clust[lowestpair[1]]
    del clust[lowestpair[0]]
    clust.append(newcluster)

  return clust[0]

# print result of hierarchical clusterization
def printclust(clust,labels=None,n=0):
  for i in range(n): print ' ',
  if clust.id<0:
    print '-'
  else:
    if labels==None: print clust.id
    else: print labels[clust.id]

  if clust.left!=None: printclust(clust.left,labels=labels,n=n+1)
  if clust.right!=None: printclust(clust.right,labels=labels,n=n+1)

# util function: height of cluster in pixels
def getheight(clust):
  if clust.left==None and clust.right==None: return 1
  return getheight(clust.left)+getheight(clust.right)

# util function: depth of cluster in pixels
def getdepth(clust):
  if clust.left==None and clust.right==None: return 0
  return max(getdepth(clust.left),getdepth(clust.right))+clust.distance

# util function: draw hierarchical node
def drawnode(draw,clust,x,y,scaling,labels):
  if clust.id<0:
    h1=getheight(clust.left)*20
    h2=getheight(clust.right)*20
    top=y-(h1+h2)/2
    bottom=y+(h1+h2)/2

    ll=clust.distance*scaling

    draw.line((x,top+h1/2,x,bottom-h2/2),fill=(255,0,0))    
    draw.line((x,top+h1/2,x+ll,top+h1/2),fill=(255,0,0))    
    draw.line((x,bottom-h2/2,x+ll,bottom-h2/2),fill=(255,0,0))        

    drawnode(draw,clust.left,x+ll,top+h1/2,scaling,labels)
    drawnode(draw,clust.right,x+ll,bottom-h2/2,scaling,labels)
  else:   
    draw.text((x+5,y-7),labels[clust.id],(0,0,0))

# draw full cluster tree
def drawdendrogram(clust,labels,gif='clusters.gif'):
  h=getheight(clust)*20
  w=1200
  depth=getdepth(clust)

  scaling=float(w-150)/depth

  img=Image.new('RGB',(w,h),(255,255,255))
  draw=ImageDraw.Draw(img)

  draw.line((0,h/2,10,h/2),fill=(255,0,0))    

  drawnode(draw,clust,10,(h/2),scaling,labels)
  img.save(gif,'GIF')


######################################################################################


def kcluster(rows,distance=pearson,k=10):
  ranges=[(min([row[i] for row in rows]),max([row[i] for row in rows])) for i in range(len(rows[0]))]
  clusters=[[random.random()*(ranges[i][1]-ranges[i][0])+ranges[i][0] for i in range(len(rows[0]))] for j in range(k)]
  lastmatches=None
  for t in range(100):
    print 'Iteration %d' % t
    bestmatches=[[] for i in range(k)]

    # find closest mean to every row
    for j in range(len(rows)):
      row=rows[j]
      bestmatch=0
      for i in range(k):
        d=getDistance(clusters[i],row,distance)
        if d<getDistance(clusters[bestmatch],row,distance): bestmatch=i
      bestmatches[bestmatch].append(j)

    if bestmatches==lastmatches: break
    lastmatches=bestmatches

    # move mean to the center of it's elements
    for i in range(k):
      avgs=[0.0]*len(rows[0])
      if len(bestmatches[i])>0:
        for rowid in bestmatches[i]:
          for m in range(len(rows[rowid])):
            avgs[m]+=rows[rowid][m]
        for j in range(len(avgs)):
          avgs[j]/=len(bestmatches[i])
        clusters[i]=avgs

  return bestmatches


######################################################################################


# place all data on a plane
def scaledown(data,distance=pearson,rate=0.01):
  n=len(data)

  realdist=[[getDistance(data[i],data[j],distance) for j in range(n)] for i in range(0,n)]

  outersum=0.0
  loc=[[random.random(),random.random()] for i in range(n)]
  fakedist=[[0.0 for j in range(n)] for i in range(n)]

  lasterror=None
  for m in range(0,1000):
    # calculating euclidian distance
    for i in range(n):
      for j in range(n):
        fakedist[i][j]=sqrt(sum([pow(loc[i][x]-loc[j][x],2) for x in range(len(loc[i]))]))

    # moving dots
    grad=[[0.0,0.0] for i in range(n)]

    totalerror=0
    for k in range(n):
      for j in range(n):
        if j==k: continue
        errorterm=(fakedist[j][k]-realdist[j][k])/realdist[j][k]

        grad[k][0]+=((loc[k][0]-loc[j][0])/fakedist[j][k])*errorterm
        grad[k][1]+=((loc[k][1]-loc[j][1])/fakedist[j][k])*errorterm

        totalerror+=abs(errorterm)
    if m%100==0: print totalerror

    if lasterror and lasterror<totalerror: break
    lasterror=totalerror

    for k in range(n):
      loc[k][0]-=rate*grad[k][0]
      loc[k][1]-=rate*grad[k][1]

  return loc


# draw plane with dots
def draw2d(data,labels,gif='mds2d.gif'):
  img=Image.new('RGB',(2000,2000),(255,255,255))
  draw=ImageDraw.Draw(img)
  for i in range(len(data)):
    x=(data[i][0]+0.5)*1000+100
    y=(data[i][1]+0.5)*1000+100
    draw.text((x,y),labels[i],(0,0,0))
  img.save(gif,'gif')